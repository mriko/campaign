//
//  DflicksApp.swift
//  Dflicks
//
//  Created by user on 14/07/21.
//

import SwiftUI

@main
struct DflicksApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
