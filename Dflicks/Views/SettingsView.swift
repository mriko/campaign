//
//  SettingsView.swift
//  Dflicks
//
//  Created by user on 14/07/21.
//

import SwiftUI

struct SettingsView: View {
    
    @Binding var isPresented: Bool
    
    @State private var selection = 1
    @State private var email = ""
    
    var body: some View {
        NavigationView {
            Form {
                Picker(selection: $selection, label: Text("Favourite Genre")) {
                    Text("Comedy").tag(1)
                    Text("Drama").tag(2)
                    Text("Action").tag(3)
                }
                
                Section(header: Text("Email")) {
                    TextField("Email", text: $email)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                }
                
                Button(action: {
                    isPresented.toggle()
                }) {
                    Text("Save")
                }
            }.navigationTitle("Setting")
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(isPresented: Binding<Bool>.constant(false))
    }
}
