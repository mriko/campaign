//
//  EnvironmentValues+ImageCache.swift
//  Dflicks
//
//  Created by user on 16/07/21.
//

import SwiftUI

struct ImageCacheKey: EnvironmentKey {
    static let defaultValue: ImageCache = TempImageCache()
}

extension EnvironmentValues {
    var imageCache: ImageCache {
        get { self[ImageCacheKey.self]}
        set { self[ImageCacheKey.self] = newValue }
    }
    
}
