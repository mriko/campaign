//
//  Review.swift
//  Dflicks
//
//  Created by user on 15/07/21.
//

import Foundation

struct ReviewResponse: Codable{
    var results: [Review]
}

struct Review: Codable, Identifiable {
    var id: String?
    var author: String?
    var content: String?
}


